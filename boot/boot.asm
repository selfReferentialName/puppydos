	; boot.asm
	; loads and boots the kernel and such

	%define gdt_base 0x500
	%define tss_base 0x600
	%define idt_base 0x800
	%define nmi_handler 0x680
	%define boot_origin 0x8000
	%define free_page 0x690

	org boot_origin
	bits 32

	; multiboot header
	; reused for miscalaneous variables
	%define mb_magic 0x1badb002
	%define mb_flags 0x00010007
mb_info: ; this spot is reused for the multiboot information structure pointer
	dd mb_magic
modules_addr:
	dd mb_flags
modules_count:
	dd 0 - mb_magic - mb_flags ; checksum
	dd boot_origin ; header_addr
	dd boot_origin ; load_addr
	dd 0 ; load_end_addr
	dd 0x20000 ; bss_end_addr -- the stack
	dd _start ; entry_addr
	dd 1 ; mode_type
	dd 80 ; width
	dd 25 ; height
	dd 0 ; depth

_start:	; set up some registers
	mov [mb_info], ebx
	mov esp, 0x20000
	pushfd
	; eflags
	pop eax
	mov ecx, eax
	and eax, 0xfff0802c
	or eax,  0x00012000
	xor eax, 0x00200000
	push eax
	popfd
	; now's also a good oportunity for cpuid detection
	pushfd
	pop eax
	xor eax, ecx
	test eax, 0x00200000 ; nz iff cpuid is supported
	jz error_no_cpuid
	; now on to control registers
	mov eax, 0x23
	mov cr0, eax
	; my version of linux leaves pce clear, so I will too
	mov eax,  0x00000490
	mov cr4, eax

	; set up the GDT
	; reminder that the address is at gdt_base
	; we're not squeezing every byte out of the boot
	; so we use a subroutine -- set_gdt
	; we actually use the null (first) descriptor as the gdtr
	mov edi, gdt_base
	mov [edi + 4], edi ; the location
	mov ax, 0xff
	mov [edi + 2], ax ; the size -- must be less than 0x8000
	; then the tss descriptor
	add edi, 8
	mov ebx, tss_base
	mov eax, ebx
	shl eax, 16
	mov ax, 0x007f
	mov [edi], eax
	mov eax, ebx
	shr eax, 16
	and ax, 0x00ff
	mov ecx, ebx
	and ecx, 0xff000000
	or eax, ecx
	or eax, 0x00008900
	mov [edi + 4], eax
	; then the normal descriptors
	add edi, 8
	mov al, 0x9a
	call set_gdt ; 0x10 -- kernel code descriptor
	add edi, 8
	mov al, 0x92
	call set_gdt ; 0x18 -- kernel data descriptor
	add edi, 8
	mov al, 0xfa
	call set_gdt ; 0x20 -- user code descriptor
	add edi, 8
	mov al, 0xf2
	call set_gdt ; 0x28 -- user data descriptor
	add edi, 8
	mov al, 0xda
	call set_gdt ; 0x30 -- i/o entrusted code descriptor
	add edi, 8
	mov al, 0xd2
	call set_gdt ; 0x38 -- i/o entrusted data descriptor
	lgdt [gdt_base + 2]

	; now set up the tss
	mov edi, tss_base
	xor eax, eax
	; null parts: link, cr3, all the registers, saved selectors, ss1, ss2
	mov [edi], eax
	add edi, 0x0c
	mov ecx, 23
	rep stosd
	; actually useful parts: esp0, ss0
	mov edi, tss_base + 0x04
	mov [edi], esp ; update each time before leaving ring 0
	mov eax, 0x18
	mov [edi + 0x04], eax

	; a little break ffrom descriptor tables to copy the NMI handler
	mov edi, nmi_handler
	mov eax, [error_nmi]
	mov [edi], eax
	mov eax, [error_nmi + 4]
	mov [edi + 4], eax
	mov eax, [error_nmi + 8]
	mov [edi + 8], eax
	mov eax, [error_nmi + 12]
	mov [edi + 12], eax

	; IDT
	; we also use a function to set this up -- set_idt
	; part one: exceptions
	mov esi, error_except
	mov edi, idt_base
	mov al, 0x8e
	call set_idt ; #DE
	add edi, 8
	call set_idt ; #DB
	add edi, 8
	mov esi, nmi_handler
	call set_idt ; NMI
	add edi, 8
	mov esi, isr_nop
	mov al, 0x8f
	call set_idt ; #BP
	add edi, 8
	mov esi, error_except
	call set_idt ; #OF
	add edi, 8
	mov al, 0x8e
	call set_idt ; #BR
	add edi, 8
	call set_idt ; #UD
	add edi, 8
	call set_idt ; #NM
	add edi, 8
	mov esi, nmi_handler
	call set_idt ; #DF
	add edi, 8
	mov esi, error_except
	mov al, 0
	call set_idt ; make a 486 act like modern processors
	add edi, 8
	mov al, 0x8e
	call set_idt ; #TS
	add edi, 8
	call set_idt ; #NP
	add edi, 8
	call set_idt ; #SS
	add edi, 8
	call set_idt ; #GP
	add edi, 8
	call set_idt ; #PF
	add edi, 16 ; skip a reserved thing
	call set_idt ; #MF
	add edi, 8
	call set_idt ; #AC
	add edi, 16 ; #MC is disabled
	call set_idt ; #XF
	; zero out everything else
	xor eax, eax
	mov [idt_base + 15 * 8], eax
	add edi, 8
	mov ecx, (256 - 20) * 2
	rep stosd
	; and done
	; now just to load stuff
	lidt [idtr_ptr]
	mov ax, 0x08
	ltr ax

	; now all the busywork is out of the way
	; time to get the information from the bootloader
	mov ebx, [mb_info]
	mov eax, [ebx] ; flags
	not eax
	test al, 0x4f ; make sure we have the needed info
	jnz error_not_enough_info

	; when we make the free list, we need to not overwrite important pages
	; here, we add all the things we need to keep
	; first off is the memory information
	; overwriting that would be very confusing
;	mov ebx, [mb_info] ; already done
	mov eax, ebx
	and eax, 0xfffff000
	mov ecx, 0x2000
	call reject_pages
	; next off is the array of modules
	mov eax, [ebx + 24]
	mov ecx, [ebx + 20]
	; and take the opportunity to save stuff
	mov [modules_addr], eax
	mov [modules_count], ecx
	shl ecx, 4
	call reject_pages
	; and then each individual module
	mov esi, [ebx + 24]
	mov edi, [ebx + 20]
	test edi, edi
	jz after_reject_each_module_loop ; skip if no modules
reject_each_module_loop:
	mov eax, [esi]
	mov ecx, [esi + 4]
	push esi
	push edi
	call reject_pages
	pop edi
	pop esi
	dec edi
	jnz reject_each_module_loop
after_reject_each_module_loop:

	; we need to align. it makes sense to put all this code here
	; sorry
	jmp setup_free_list

set_gdt:; set the gdt descriptor at edi to have:
	; a base of 0 and segment limit that covers the entire address space
	; and type, s, dpl, and p bits in al
	push ebx
	mov ebx, 0x0000ffff
	mov [edi], ebx
	mov ebx, 0x00cf0000
	mov [edi + 4], ebx
	mov [edi + 5], al
	pop ebx
	ret

set_idt:; set the idt descriptor at edi to have:
	; a type, s, dpl, and p of al
	; kernel interupts have an al of 0x8e
	; kernel traps have an al of 0x8f
	; a base of esi
	; and a code segment of bx
	push ecx
	push esi
	mov ecx, ebx
	shl ecx, 16
	and esi, 0x0000ffff
	or ecx, esi
	mov [edi], ecx
	pop esi
	mov [edi + 4], esi
	mov cl, al
	shl ax, 8
	mov [edi + 4], ax
	pop ecx
	ret

idtr_ptr:
	dw 0x800
	dd idt_base

isr_nop:; does absolutely nothing but returns from an isr
	iret

logs:	; log the string at esi
	pushad
	mov dx, 0xe9
	mov al, [esi]
logs_loop:
	outsb
	mov al, [esi]
	test al, al
	jnz logs_loop
	popad
	ret

error_no_cpuid_msg:
	db "Error: you have an ancient CPU with no CPUID support.", 0x0a, 0
error_no_cpuid:
	mov esi, error_no_cpuid_msg
	call logs
	jmp stop

error_except_msg:
	db "Error: the bootstrapper had an exception.", 0x0a, 0
error_except:
	mov esi, error_except_msg
	call logs
	jmp stop

error_not_enough_info_msg:
	db "Error: the bootloader gave too little information.", 0x0a, 0
error_not_enough_info:
	mov esi, error_not_enough_info_msg
	call logs
	jmp stop

error_nmi:
	; this must be exactly 16 bytes long
	; we copy this to its final position
	mov dx, 0xe9
	mov al, 'N'
	out dx, al
	mov al, 'M'
	out dx, al
	mov al, 'I'
	out dx, al
	hlt
	jmp $ - 1 ; hlt

stop:	cli
	hlt
	jmp stop

reject_pages:
	; add ecx bytes starting at eax to the list of rejected pages
	dec ecx
	shr ecx, 12
	inc ecx
	and ax, 0xf000
reject_pages_loop:
	call reject_page
	add eax, 0x1000
	loop reject_pages_loop
	ret

reject_page:
	; add the page at eax to the list of rejected pages
	push edi
	push ebx
	mov bx, 0xfff
	mov edi, rejected_pages_other - 4
reject_page_loop:
	add edi, 4
	test [edi], bx
	jz reject_page_loop
	; edi is an unused entry
	mov [edi], eax
	pop ebx
	pop edi
	ret

	; padd out to avoid overwriting active code
	; apologies for the previous code -- JUMP_TO_SETUP_FREE_LIST
	align 4096
	nop
	nop
	nop
	nop
setup_free_list:
	xor eax, eax
	mov [free_page], eax ; terminate the free list
	mov ebx, [mb_info]
	; gave up on using a memory map
	; for now, we'll just use low memory
	mov ecx, [ebx + 4]
	shr ecx, 2
	xor esi, esi
setup_free_list_loop:
	mov edi, rejected_pages
setup_free_list_reject_loop:
	mov edx, [edi]
	add edi, 0x1000
	cmp esi, edx
	je setup_free_list_continue ; found page to reject
	test edx, 0xfff
	jz setup_free_list_reject_loop
	; found page to add
	mov [esi], eax
	mov [free_page], esi
	mov eax, esi
setup_free_list_continue:
	add esi, 0x1000
	dec ecx
	jc setup_free_list_loop_break
	jnz setup_free_list_loop
setup_free_list_loop_break:

	; now we make the global page stuff
	; global pages are at 0xc0000000
	; the page directory is at 0xc0040000
	; and page tables are at 0xc0040000
	; first, we get a page directory
	call get_page
	mov ebp, eax
	; global page table
	call get_page
	mov esi, eax
	or eax, 0x003 ; supervisor writeable cached
	mov [ebp + 0xc00], eax
	mov eax, 0x6e72656b
	call map_module
	mov eax, 0x0003
	mov [esi + 8], eax
	; put the page directory in
	call get_page
	mov esi, eax
	or eax, 0x003
	mov [ebp + 0xc04], eax
	mov eax, ebp
	or eax, 0x003
	mov [esi], eax
	; and the page tables
	mov [ebp + 0xc08], eax
	; also put in stuff to boot
	call get_page
	mov esi, eax
	or eax, 0x007 ; user writeable
	mov [ebp], eax ; page directory
	mov eax, 0x3003
	mov [esi + 8], eax ; the page itself
	mov [esi + 12], eax ; why is this 0x3000

	; get ready to jump to the kernel
	mov eax, 0x6e72656b
	call find_module
	push eax
	call get_page
	mov esi, eax
	or eax, 0x3
	mov [ebp + 0xc00], eax ; put in a kernel page table
	pop eax
	mov edi, eax
	or edi, 0x3
	mov [esi], edi ; and the kernel itself
	mov edi, 0x0003
	mov [esi + 8], edi ; and a much needed page
	mov eax, [eax]

	mov eax, ebp
	mov eax, [ebp]
	and ax, 0xf000
	mov eax, [eax + 8]

	mov cr3, ebp
	mov esi, paged_payload
	mov ecx, 128
	mov edi, 0x3000
	rep movsb
	mov eax, cr0
	or eax, 0x80000000
	jmp 0x3000

paged_payload: ; must be smaller than 128 bytes
	; set pageing
	mov cr0, eax
	; load virtually addressed gdt
	mov eax, gdt_base + 0xc0002000
	mov [0x3ffc], eax
	mov ax, 0x40
	mov [0x3ffa], ax
	lgdt [0x3ffa]
	; and idt
	mov eax, idt_base + 0xc0002000
	mov [0x3ff8], eax
	mov ax, 0x7ff
	mov [0x3ff6], ax
	lidt [0x3ff6]
	; and make the jump
	mov edx, [0xc0000018]
	jmp edx

map_module:
	; add the module whose name starts with eax to the page directory
	; at esi with mask ebx
	; trashes eax and esi
	push ecx
	call find_module
	dec ecx
	shr ecx, 12
	inc ecx
	push edx
map_module_loop:
	mov edx, eax
	or edx, ebx
	mov [esi], edx
	loop map_module_loop
	pop edx
	pop ecx
	ret

find_module:
	; find the module with a name that starts with eax
	; returns address in eax
	; returns size in ecx
	; TODO: check that it actually is loaded
	push esi
	push ebx
	mov esi, [modules_addr]
	sub esi, 16
find_module_loop:
	add esi, 16
	mov ebx, [esi + 8]
	cmp [ebx], eax
	jne find_module_loop
	pop ebx
	mov eax, [esi]
	mov ecx, [esi + 4]
	pop esi
	ret

get_page:
	; returns output in eax
	mov eax, [free_page]
	push ebx
	mov ebx, [eax]
	mov [free_page], ebx
	pop ebx
	ret

logx:	; log the hex value at eax
	push eax
	push ecx
	mov ecx, 8
	push ebx
	mov ebx, eax
logx_loop:
	rol ebx, 4
	mov eax, ebx
	and eax, 0x0f
	mov al, [logx_lookup + eax]
	out 0xe9, al
	loop logx_loop
	mov al, 0x0a
	out 0xe9, al
	pop ebx
	pop ecx
	pop eax
	ret

logx_lookup:
	db '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
	db 'a', 'b', 'c', 'd', 'e', 'f'

	align 4096
	dd 0
rejected_pages:
	; any pages from here to the next entry with bits set in 0xfff
	; is a page not to put in the free list
	dd 0x0000
	dd 0x3000
	dd 0x10000
	dd 0x11000
	dd 0x12000
	dd 0x13000
	dd 0x14000
	dd 0x15000
	dd 0x16000
	dd 0x17000
	dd 0x18000
	dd 0x19000
	dd 0x1a000
	dd 0x1b000
	dd 0x1c000
	dd 0x1d000
	dd 0x1e000
	dd 0x1f000
rejected_pages_other:
	; space for other stuff
	align 4096, db 0xff
