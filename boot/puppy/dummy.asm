	; just a dummy kernel to test the toolchain

section .text
global _start
_start:
	mov ax, 0x18
	mov ds, ax
	mov al, '@'
	mov dx, 0xe9
	out dx, al
	hlt
	jmp $ - 1
