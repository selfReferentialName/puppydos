	; dump_info.asm
	; a development tool to get 

	%define boot_origin 0x8000

	org boot_origin
	bits 32

	; multiboot header
	; reused for miscalaneous variables
	%define mb_magic 0x1badb002
	%define mb_flags 0x00010007
mb_info: ; this spot is reused for the multiboot information structure pointer
	dd mb_magic
modules_addr:
	dd mb_flags
modules_count:
	dd 0 - mb_magic - mb_flags ; checksum
	dd boot_origin ; header_addr
	dd boot_origin ; load_addr
	dd 0 ; load_end_addr
	dd 0x20000 ; bss_end_addr -- the stack
	dd _start ; entry_addr
	dd 1 ; mode_type
	dd 80 ; width
	dd 25 ; height
	dd 0 ; depth

_start:	; set up some registers
	mov [mb_info], ebx
	mov esp, 0x20000
	pushfd
	; eflags
	pop eax
	mov ecx, eax
	and eax, 0xfff0802c
	or eax,  0x00012000
	xor eax, 0x00200000
	push eax
	popfd
	; now on to control registers
	mov eax, 0x23
	mov cr0, eax
	; my version of linux leaves pce clear, so I will too
	mov eax,  0x00000490
	mov cr4, eax

	mov ebx, [mb_info]
	mov eax, msg_mb_info
	call logs
	mov eax, ebx
	call logx
	mov eax, msg_mb_flags
	call logs
	mov eax, [ebx]
	call logx
	mov eax, msg_mb_cmdline
	call logs
	mov eax, [ebx + 16]
	call logs
	mov eax, msg_mb_mods_count
	call logs
	mov eax, [ebx + 20]
	call logx
	mov eax, msg_mb_mods_addr
	call logs
	mov eax, [ebx + 24]
	call logx
	mov esi, [ebx + 24]
	mov ecx, [ebx + 20]
dump_mods_loop:
	mov eax, msg_mod_start
	call logs
	mov eax, [esi]
	call logx
	mov eax, msg_mod_size
	call logs
	mov eax, [esi + 4]
	sub eax, [esi]
	call logx
	mov eax, msg_mod_string_addr
	call logs
	mov eax, [esi + 8]
	call logx
	mov eax, msg_mod_string
	call logs
	mov eax, [esi + 8]
	call logs
	add esi, 16
	loop dump_mods_loop
	mov eax, msg_mb_mmap_length
	call logs
	mov eax, [ebx + 44]
	call logx
	mov eax, msg_mb_mmap_addr
	call logs
	mov eax, [ebx + 48]
	call logs
	mov esi, [ebx + 48]
	mov ecx, [ebx + 44]
	mov eax, msg_mmap_size
	call logs
	mov eax, [esi]
	mov edx, eax
	call logx
	add esi, 4
dump_mmap_loop:
	mov eax, msg_mmap_addr
	call logs
	mov eax, [esi + 4]
	call logx
	mov eax, [esi]
	call logx
	mov eax, msg_mmap_length
	call logs
	mov eax, [esi + 12]
	call logx
	mov eax, [esi + 8]
	call logx
	mov eax, msg_mmap_type
	call logs
	mov eax, [esi + 16]
	call logx
	add esi, edx
	sub ecx, edx
	jz dump_mmap_done
	jnc dump_mmap_loop
dump_mmap_done:
	hlt
	jmp $ - 1

msg_mb_info:
	db "The multiboot header is located at: ", 0
msg_mb_flags:
	db 0x0a, "With flags: ", 0
msg_mb_cmdline:
	db 0x0a, "With command line: ", 0
msg_mb_mods_count:
	db 0x0a, "Count of loaded modules: ", 0
msg_mb_mods_addr:
	db 0x0a, "Starting at: ", 0
msg_mod_start:
	db 0x0a, "	Module start location: ", 0
msg_mod_size:
	db 0x0a, "	Module size: ", 0
msg_mod_string_addr:
	db 0x0a, "	Module string address: ", 0
msg_mod_string:
	db 0x0a, "	Module string itself: ", 0
msg_mb_mmap_length:
	db 0x0a, "With memmap of length: ", 0
msg_mb_mmap_addr:
	db 0x0a, "Starting at: ", 0
msg_mmap_size:
	db 0x0a, "Memmap entry size: ", 0
msg_mmap_addr:
	db 0x0a, "	Memmap entry address: ", 0
msg_mmap_length:
	db 0x0a, "	Memmap entry length: ", 0
msg_mmap_type:
	db 0x0a, "	Memmap entry type: ", 0

logx:	; log the hex value at eax
	push ecx
	mov ecx, 8
	push ebx
	mov ebx, eax
logx_loop:
	rol ebx, 4
	mov eax, ebx
	and eax, 0x0f
	mov al, [logx_lookup + eax]
	out 0xe9, al
	loop logx_loop
	pop ebx
	pop ecx
	ret

logx_lookup:
	db '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
	db 'a', 'b', 'c', 'd', 'e', 'f'

logs:	; log the string at eax
	; trashes eax
	push ebx
	mov ebx, eax
logs_loop:
	mov al, [ebx]
	out 0xe9, al
	inc ebx
	test al, al
	jnz logs_loop
	pop ebx
	ret
